# Phyre Google Pay module

The following library provides a simplified API for interacting with the Google Pay's wallet on an Android device and incorporates generation of push provisioning payload via Phyre's REST API.

API documentation can be found [here](https://phyreapp.gitlab.io/phyre-gpay).

## Installation

Declare the repository
```groovy
repositories {
    maven {
        url 'https://gitlab.com/api/v4/projects/35733902/packages/maven'
        credentials(HttpHeaderCredentials) {
            name = 'Deploy-Token'
            value = "U66hQk6idw8z2BxY7mvG"
        }
        authentication {
            header(HttpHeaderAuthentication)
        }
    }
}
```

And in your module's dependencies add:
```groovy
dependencies {
    ...
    implementation "com.phyreapp:phyre-gpay:3.0.1"
    implementation <Google Pay Dependency>
    ...
}
```

**Note:** You must also include Google Pay's `aar` file in the same module in which you have included `phyre-gpay`. It can be downloaded from Google Pay's documentation.

## Setup

### Application

In your `Application` class call:

```kotlin
PhyreGPayClient.init(
    context = context, 
    config = phyreGPayClientConfig { 
        debug = true /* for debug logs */ 
    },
)
```

### AndroidManifest.xml
```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    package="com.phyreapp.mpsdk">

    ...
    
    <!-- Permission to access internet - required -->
    <uses-permission android:name="android.permission.INTERNET"/>
    
    ...

</manifest>
```


## Usage

### Obtaining `PhyreGPayService`:

```kotlin
val gpayService = PhyreGPayClient.gpayService
```

### Check Google Pay availability

```kotlin
coroutineScope.launch {
    when (gpayService.isGooglePayAvailable()) {
        is Error<GooglePayEligibilityCheckError> -> TODO()
        is Success<Boolean> -> isGPayAvailable.value // true/false
    }
}
```

### Reading Card Token State:

```kotlin
coroutineScope {
    val gpayCardStatus = gpayService.getCardStatus(
        fpanLast4,
        cardProcessor,
    )

    when (tokenStateResult) {
        is Success<GPayCardStatus> -> {
            when (val cardStatus = gpayCardStatus.value) {
                is GPayCardStatus.NeedsIdentityVerification -> TODO() // show "Added to GPay"
                is GPayCardStatus.Provisioned -> TODO() // show "Added to GPay"
                GPayCardStatus.NotProvisioned -> TODO()
                GPayCardStatus.Unavailable -> TODO()
            }
        } 
        is Error<GPayError> -> {
            // ...
        }
    }    
}
```

### Tokenizing

In your `Activity` or `Fragment`'s `onCreate()`:

```kotlin
val tokenizeLauncher = PhyreGPayClient.getPushProvisioningLauncher(this@MainActivity) { result ->
    when (result) {
        ActionResult.Canceled -> {
            
        }
        is ActionResult.Success -> {
            
        }
        is ActionResult.Error -> {
            when (val error: GPayTokenizationError = result.value) {
                is GPayError -> {

                }
                is GetGPayProvisioningPayloadError -> {

                }
            }
        }
    }
}
```

#### Launch tokenization process

```kotlin
val clientId = "<Your Client ID>"
val authenticationId = "<Your authentication ID or token>"
tokenizeLauncher.launch(
    clientId,
    authenticationId,
    clientPaymentCardId, // Paynetics Payment Card ID
    cardProcessor, // eg. Visa, Mastercard, etc.
    fpanLast4: String, // Card's number last four digits 
    cardholderNames, // Cardholder's names on the card
)
```

### Set card as default payment method in Google Pay Wallet

A user's Google Pay Wallet can have multiple cards from different issuers added to it. Only one card can be set as default payment method for paying by NFC. The developer can provide an option for the user to set the card issued through that specific app to be set as the default one for NFC payments.

In your `Activity` or `Fragment`'s `onCreate()`:

```kotlin
val setDefaultCardLauncher = PhyreGPayClient.getSetDefaultCardInGPayWalletLauncher(this) { result ->
    when (result) {
        ActionResult.Canceled -> TODO()
        is ActionResult.Error -> TODO()
        is ActionResult.Success -> TODO()
    }
}
```

```kotlin
setDefaultCardLauncher.launch()
```

### Google Pay as default NFC Payment method on the device

Android allows multiple apps to serve as NFC Payment Apps (not just Google Pay). However only one payment app can be set as default. 

#### Check if Google Pay is the default NFC Payment App

```kotlin
val isGPayDefault: Boolean = gpayService.isGooglePayTheDefaultNFCPaymentApp()
```

#### Set Google Pay as default NFC Payment App

In your `Activity` or `Fragment`'s `onCreate()`:

```kotlin
val setDefaultPaymentAppLauncher = PhyreGPayClient.getSetGPayAsDefaultNFCPaymentAppLauncher(this) { result ->
    when (result) {
        ActionResult.Canceled -> TODO()
        // The type is ActionResult.Error<Nothing>. The operation failed. No additional details are available.
        is ActionResult.Error -> TODO()
        // Type is ActionResult.Success<ActionResult.Success.Default>. No additional details are available.
        is ActionResult.Success -> TODO()
    }
}
```

```kotlin
if (isGPayDefault.not()) {
    setDefaultPaymentAppLauncher.launch()
}
```

##

The underlying implementation of the launchers uses Android's [Activity Result Contracts](https://developer.android.com/training/basics/intents/result). To unregister the callback, you can simply call `tokenizeLauncher.unregister()`.

## Data types

### GPayCardStatus

```kotlin
sealed interface GPayCardStatus: Parcelable {

    companion object {}

    /**
     * The card is added to the Google Pay Wallet.
     */
    data class Provisioned(
        /**
         * The card is set as the default one for payments with Google Pay Wallet.
         */
        val isDefaultInGPayWallet: Boolean,
    ) : GPayCardStatus {
        companion object {}
    }

    /**
     * The card is in the active wallet, but requires additional user authentication for use
     * (yellow path step-up).
     */
    data class NeedsIdentityVerification(
        /**
         * The card is set as the default one for payments with Google Pay Wallet.
         */
        val isDefaultInGPayWallet: Boolean,
    ): GPayCardStatus {
        companion object {}
    }

    /**
     * The card is not added to the Google Pay Wallet.
     */
    data object NotProvisioned: GPayCardStatus

    /**
     * Card cannot be provisioned.
     */
    data object Unavailable: GPayCardStatus

}
```

### CardProcessor

```kotlin
enum CardProcessor {
    AMEX,
    DISCOVER,
    MASTERCARD,
    VISA,
    INTERAC,
    EFTPOS,
    MAESTRO,
    JCB,
    ELO,
    MIR,
}
```

### Errors

```kotlin
/**
 * Base Google Pay Wallet operation error. Groups together [GPayError] and [NoActiveGPayWalletError].
 */
sealed interface GPayWalletOperationError : Parcelable {

    companion object {}

}

/**
 * Errors that can occur during the tokenization process. Can be returned as an error result from
 * [PhyreGPayClient.getPushProvisioningLauncher].
 */
sealed interface GPayTokenizationError : Parcelable {

    companion object {}

}

/**
 * Groups together [ClientNotAvailableError], [InsecureDeviceError] and [GenericError]. Also
 * represents [GPayWalletOperationError] and [GPayTokenizationError]. Can be returned on its own by
 * [GPayService.getCardStatus].
 */
sealed interface GPayError : GPayWalletOperationError, GPayTokenizationError {

    companion object {}

}

/**
 * Error returned by [GPayService.isGooglePayAvailable].
 */
sealed interface GooglePayEligibilityCheckError {

    companion object {}

}

/**
 * Mapped from [TapAndPayStatusCodes.TAP_AND_PAY_UNAVAILABLE].
 * Represents [GPayError] and [GPayTokenizationError].
 */
data class ClientNotAvailableError(
    val cause: Throwable,
    val message: String,
) : GPayError, GPayTokenizationError {

    companion object { }

}

/**
 * Mapped from [TapAndPayStatusCodes.TAP_AND_PAY_ATTESTATION_ERROR].
 * Represents [GPayError], [GPayTokenizationError] and [GooglePayEligibilityCheckError].
 */
data class InsecureDeviceError(
    val cause: Throwable,
    val message: String,
) : GPayError, GPayTokenizationError, GooglePayEligibilityCheckError {

    companion object { }

}

/**
 * Any unknown or unexpected error.
 */
data class GenericError(
    val cause: Throwable?,
    val message: String?,
) : GPayError, GPayTokenizationError, GooglePayEligibilityCheckError {

    companion object { }

}

/**
 * Mapped from [TapAndPayStatusCodes.TAP_AND_PAY_NO_ACTIVE_WALLET].
 * Represents [GPayWalletOperationError].
 */
internal data class NoActiveGPayWalletError(
    val cause: Throwable,
    val message: String,
) : GPayWalletOperationError {

    companion object {}

}

/**
 * Can result from failure of obtaining the push provisioning payload during tokenization.
 */
data class GetGPayProvisioningPayloadError(val message: String) : Parcelable, GPayTokenizationError {

    companion object { }

}
```

# Release Notes

## v3.x.x
- Updated Kotlin to 1.9.10.
- Compiled against JVM 17
- Version is built using Play Services Tap And Pay 18.3.3

## v2.x.x
- Provide API for setting Google Wallet as default NFC payment method on the device.
- Provide API to check whether Google Pay is the default NFC Payment App on the device.
- Provide API for setting a card as the default one in the Google Pay Wallet.
- Using representing a card status in the Google Pay Wallet using a sealed interface instead of an `enum class`.
- Provide information whether a tokenized card is the default one in the Google Pay Wallet.

## v1.2.0
- Updated Kotlin to 1.8.20
- Version is build using Play Services Tap And Pay 18.3.2
    - Nothing introduced with this version is being used, so this isn't a breaking change.
- Uptaded internal dependency versions
